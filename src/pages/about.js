import React from "react"

import Layout from "../components/layout"

const about = () => {
  return (
    <Layout>
      <p>
        Lorem ipsum dolor sit amet consectetur, adipisicing elit. Iste ipsam
        vitae eaque maxime ea cupiditate animi dolor molestias quidem voluptate,
        voluptatibus ex laboriosam temporibus atque voluptatum porro deserunt
        tempore dicta dolore! Ipsa pariatur quis similique debitis reiciendis!
        Explicabo, voluptatum porro quasi laboriosam voluptates excepturi
        placeat distinctio, corporis, eius eveniet praesentium nihil saepe error
        fugiat quam accusamus dolores nostrum vitae eligendi! Magni vero magnam
        quod exercitationem ipsum, nam reprehenderit vel voluptate nemo
        laboriosam nihil quam fugit consequatur ea beatae facilis quas?
      </p>
    </Layout>
  )
}

export default about
