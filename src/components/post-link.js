import React from "react"
import { Link } from "gatsby"

import styles from "./post-link.module.scss"

const PostLink = ({ post }) => (
  <div className={styles.container}>
    <img src={post.frontmatter.thumbnail} alt="" />
    <Link to={post.frontmatter.path}>
      {post.frontmatter.title} ({post.frontmatter.date})
    </Link>
  </div>
)

export default PostLink
