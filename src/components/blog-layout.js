import React from "react"
import styles from "./blog-layout.module.scss"

const BlogLayout = ({ children }) => {
  return <div className={styles.container}>{children}</div>
}

export default BlogLayout
