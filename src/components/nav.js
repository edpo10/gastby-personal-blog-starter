import React from "react"
import { Link } from "gatsby"

import styles from "./nav.module.scss"

const nav = ({ links }) => (
  <nav>
    <ul className={styles.container}>
      {links.map(link => (
        <li>
          <Link to={link}>{link}</Link>
        </li>
      ))}
    </ul>
  </nav>
)

export default nav
