import { Link } from "gatsby"
import PropTypes from "prop-types"
import React from "react"
import Img from "gatsby-image"
import { useStaticQuery, graphql } from "gatsby"

import Nav from "./nav"
import styles from "./header.module.scss"

const Header = ({ data }) => {
  console.log(data)
  return (
    <header className={styles.header}>
      <Link
        to="/"
        style={{
          color: `white`,
          textDecoration: `none`,
        }}
      ></Link>
      <Nav links={["about", "contact"]} />
    </header>
  )
}

export const query = graphql`
  query {
    file(relativePath: { eq: "gatsby-astronaut.png" }) {
      childImageSharp {
        fluid {
          aspectRatio
          base64
          sizes
          src
          srcSet
        }
      }
    }
  }
`

Header.propTypes = {
  siteTitle: PropTypes.string,
}

Header.defaultProps = {
  siteTitle: ``,
}

export default Header
